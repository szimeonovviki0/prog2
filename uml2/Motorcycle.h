#include <exception>
#include <string>
using namespace std;

#ifndef __Motorcycle_h__
#define __Motorcycle_h__

#include "Vehicle.h"

// class Vehicle;
class Motorcycle;

class Motorcycle: public Vehicle
{
	private: string _motorType;

	public: string getType();

	public: void setType(string aType);

	public: void makeSound();
};

#endif
