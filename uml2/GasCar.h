#include <exception>
#include <string>
using namespace std;

#ifndef __GasCar_h__
#define __GasCar_h__

#include "Car.h"

// class Car;
class GasCar;

class GasCar: public Car
{
	private: string _fuelType;
	private: double _tankSize;

	public: string getFuel();

	public: void setFuel(string aFuel);

	public: double getTank();

	public: void setTank(double aSize);
};

#endif
